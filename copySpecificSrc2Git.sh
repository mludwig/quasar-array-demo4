#!/bin/csh
# copy only the specific sources (excluding quasar) to my specific git repo,
# so that jenkins can pick it up and build a array test server against
# a specific branch of quasar
#
# check out demo4 git
set gitspecific="https://gitlab.cern.ch/mludwig/quasar-arrays-tester.git"
set src=`pwd`
mkdir ./xxx
cd ./xxx
echo "-----------------------------------"
echo "cloning specific code demo4 into "`pwd`
echo "-----------------------------------"
git clone ${gitspecific}
cd quasar-arrays-tester
set targ=`pwd`
#
echo "-----------------------------------"
echo "copying code from "${src}" to " ${targ}
echo "-----------------------------------"
cp -v ${src}/Design/Design.xml ${targ}/Design
cp -v ${src}/Device/src/DDEMO4.cpp ${targ}/Device/src
cp -v ${src}/Device/include/DDEMO4.h ${targ}/Device/include
cp -v ${src}/Server/src/QuasarServer.cpp ${targ}/Server/src
cp -v ${src}/Server/include/QuasarServer.h ${targ}/Server/include
#
mkdir -p ${targ}/ThreadLauncher/src
mkdir -p ${targ}/ThreadLauncher/include
cp -rv  ${src}/ThreadLauncher/* ${targ}/ThreadLauncher
#
cp -v ${src}/CMakeLists.txt ${targ}
cp -v ${src}/ProjectSettings.cmake ${targ}
cp -v ${src}/ci-x64-cc7_ua_build.cmake ${targ}
cp -v ${src}/bin/config.xml ${targ}/bin
#
#

echo "-----------------------------------"
echo "checking back into git specific "${gitspecific}
echo "-----------------------------------"
cd ${targ}
pwd
git status
git add .
git commit -m "update from eclipse"
git push origin master
#
# cleanup
cd ${targ}
cd ../..
echo "cleaning up "`pwd`/xxx
rm -rf ./xxx

