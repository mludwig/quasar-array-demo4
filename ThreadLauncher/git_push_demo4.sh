#!/bin/csh
# git push quasar project demo4 to private gitlab, without the fwk overhead
# since the demo is a subdir of quasar (branch), we have
# to copy it into a seperate buffer directory quasar-array-demo4-buffer
# to keep demo and fwk separate
pwd
ls
cp -rv ./* $1
pwd
cd $1
pwd

# now git push
git status



find ./ -name "CMakeCache.txt" -exec rm {} \;
find ./ -name "CMakeFiles" -exec rm -rf {} \;
rm -rf CMakeFiles
rm -rf html
rm -rf latex
# come up with a message for git
set MSG1=`date`" eclipse-push from "`whoami`"@"`hostname`
echo ${MSG1}
rm -f ./tmp_msg.txt
echo ${MSG1}"\n" > ./tmp_msg.txt
gedit ./tmp_msg.txt
set MSG=`cat ./tmp_msg.txt`

git add --all
git commit -m "${MSG}"
git push origin master



