
	
	add_custom_command(OUTPUT include/DRoot.h src/DRoot.cpp
	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
	COMMAND python quasar.py generate root
	DEPENDS ${DESIGN_FILE} ${PROJECT_SOURCE_DIR}/quasar.py validateDesign designToRootHeader.xslt designToRootBody.xslt
	)
	
	
	
	add_custom_command(OUTPUT ${PROJECT_SOURCE_DIR}/Device/generated/Base_DDEMO4.h ${PROJECT_SOURCE_DIR}/Device/generated/Base_DDEMO4.cpp 
	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
	COMMAND python quasar.py generate base DEMO4
	DEPENDS ${DESIGN_FILE} ${PROJECT_SOURCE_DIR}/quasar.py ${PROJECT_SOURCE_DIR}/Device/designToDeviceBaseHeader.xslt Configuration.hxx validateDesign ${PROJECT_SOURCE_DIR}/Device/designToDeviceBaseBody.xslt
	)	
	
	
	
	
	add_custom_command(OUTPUT ${PROJECT_SOURCE_DIR}/Device/generated/Base_DReadoutArrays.h ${PROJECT_SOURCE_DIR}/Device/generated/Base_DReadoutArrays.cpp 
	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
	COMMAND python quasar.py generate base ReadoutArrays
	DEPENDS ${DESIGN_FILE} ${PROJECT_SOURCE_DIR}/quasar.py ${PROJECT_SOURCE_DIR}/Device/designToDeviceBaseHeader.xslt Configuration.hxx validateDesign ${PROJECT_SOURCE_DIR}/Device/designToDeviceBaseBody.xslt
	)	
	
	
	
	
	set(DEVICEBASE_GENERATED_FILES
        include/DRoot.h
        src/DRoot.cpp
	
	generated/Base_DDEMO4.h
	generated/Base_DDEMO4.cpp
	
	generated/Base_DReadoutArrays.h
	generated/Base_DReadoutArrays.cpp
	
	)
	
	set(DEVICE_CLASSES
	
	src/DDEMO4.cpp
	
	src/DReadoutArrays.cpp
	
	)

	add_custom_target(DeviceBase DEPENDS ${DEVICEBASE_GENERATED_FILES} )
	
	add_custom_target(DeviceGeneratedHeaders DEPENDS include/DRoot.h ${DEVICEBASE_GENERATED_FILES})
	
	