/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * DDEMO4 server demonstrating new array data types.
 * It reads in the config using a class method of DDEMO4, then
 * launches a thread per device, initializes class DDEMO4 instances and goes
 * into an update loop. The loop ends after some 500 calls on its own, this is
 * enough time to do some interactive tests.
 */
#include "QuasarServer.h"
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/foreach.hpp>

#include <LogIt.h>
#include <string.h>
#include <shutdown.h>
#include "../../Device/include/DDEMO4.h"

#include "ThreadLauncher.h"
#include "Configurator.h"

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
    	boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
    }
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing Quasar server, reading configuration.";
	Device::DDEMO4::testReadConfig();

	// start update threads for all objects
	BOOST_FOREACH(Device::DDEMO4* dev, Device::DRoot::getInstance()->demo4s())
	{
		// have each device on its own thread: tell the thread which dev, and the device which thread.
		boost::shared_ptr<ThreadLauncher> threadlauncher(new ThreadLauncher());
		threadlauncher->setDDEMO4( dev );
		dev->setThreadLauncher( threadlauncher );
		LOG(Log::INF) << "starting thread for " << dev->getFullName();
		threadlauncher->start();
	}

}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server.";
}

void QuasarServer::initializeLogIt()
{
	Log::initializeLogging();
  LOG(Log::INF) << "Logging initialized.";
}
