#ifndef THREADLAUNCHER_INCLUDE_THREADLAUNCHER_H_
#define THREADLAUNCHER_INCLUDE_THREADLAUNCHER_H_

#include <set>
#include <boost/thread.hpp>

#include "../../Device/include/DDEMO4.h"

class DDEMO4;

class ThreadLauncher
{
public:
	ThreadLauncher();
	virtual ~ThreadLauncher();

	void start();
	void stop();
	void setDDEMO4( Device::DDEMO4 *d ){ m_ddemo4 = d; }

	// needed for threads
	void operator()();

private:
	Device::DDEMO4 * m_ddemo4;

	const std::set<DDEMO4*> m_devices;
	boost::thread m_thread;

	bool m_scheduleStopRunning;
	bool m_isRunning;
	uint32_t m_incrementPeriodMs;
};


#endif /* THREADLAUNCHER_INCLUDE_THREADLAUNCHER_H_ */
