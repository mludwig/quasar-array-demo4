README.txt

the quasar fwk is in github.com branch arrays4
the demo4 specific test code is in gitlab master quasar-array-demo4 (including quasar top level)

the workflow (using bootstrap.sh):

== bootstrap.sh ==
0. clone fwk branch arrays4 into workdir "quasar fwk" this is a edit-push-only 
2. clone demo4 into buffer directory
3. from workdir, use fwk to create a project
4. "magically" write the specific project part by copying the neccessary files from buffer
5. when quasar creates the project we need to do the editor merge for the specific code
==bootstrap.sh end==


== the hand part using eclipse targets==
5. edit fwk in workdir, and project in sub-workdir
6. run, test, when OK
7. hand-transfer the needed fwk changes into fwk-only clone  "quasar fwk", push to branch
8. push whole project including quasar and project subdir to gitlab
== hand part end==

9. then, refresh framework and project by executing bootstrap.sh again



log of modifications
====================

(1)------
AS set_cache_array... needs to be generated for OpcUa_Double **arrays** with the correct array signature;

ASDEMO4.h: 
UaStatus setCache_array_double (const UaVariant & value, OpcUa_StatusCode statusCode,const UaDateTime & srcTime = UaDateTime::now()) ;

ASDEMO4.cpp:
UaStatus ASCARRAYTEST::setCache_array_Double (const UaVariant & value, OpcUa_StatusCode statusCode,const UaDateTime & srcTime )
{
// debug: setter
    UaVariant v = value;

    UaUInt32Array arrayDimensions;
    v.arrayDimensions( arrayDimensions);
    OpcUa_Int32 dimSize = v.dimensionSize();
    m_cache_array_Double->setValueRank( dimSize );
    m_cache_array_Double->setArrayDimensions( arrayDimensions );

    // double array
    m_cache_array_Double->setDataType( UaNodeId( OpcUaId_Double, /* system namespace */ 0 ));

    return m_cache_array_Double->setValue (0, UaDataValue (v, statusCode, srcTime, UaDateTime::now()), /*check access*/OpcUa_False  ) ;
}
==>> modify AddressSpace/designToClassBody.xslt
==>> modify Design/CommonFunctions.xslt

needed to have an xslt switch to detect if a variable is a scalar or an array, so that different GET/SET headers can be generated. 
<xsl:function name="fnc:varSetter"> and
<xsl:function name="fnc:varSetterArray"> are used, the switch is in
<xsl:for-each select="d:cachevariable">
where it checks for the presence of a sub element array <xsl:when test="d:array">

.. clarify:
1-user format for arrays. Have vectors presently, could also have UaVariants ASDEMO4.cpp. USE VECTORS
2-finish code for cachevars and test it
-cache get-set scalar and arrays all types, with client, error, max bounds of serializer


3-source vars and config vars


source variables
================
sth VERY different from chae vars
they are not cached at all, this is HW interaction
can be synchronous (=same thread) or asynchronous (=extra thread) for read or write, protected by mutex

declaration in Design.xml i.e:
    <d:sourcevariable dataType="UaString" name="diagnostics" addressSpaceRead="asynchronous" addressSpaceReadUseMutex="no" addressSpaceWrite="forbidden" addressSpaceWriteUseMutex="no"/>
    <d:sourcevariable dataType="UaByteString" name="write" addressSpaceRead="synchronous" addressSpaceReadUseMutex="no" addressSpaceWrite="asynchronous" addressSpaceWriteUseMutex="no"/>
    <d:sourcevariable dataType="UaString" name="diagnostics" addressSpaceRead="asynchronous" addressSpaceReadUseMutex="no" addressSpaceWrite="forbidden" addressSpaceWriteUseMutex="no"/>
    <d:sourcevariable dataType="OpcUa_Boolean" name="value" addressSpaceRead="synchronous" addressSpaceReadUseMutex="no" addressSpaceWrite="asynchronous" addressSpaceWriteUseMutex="of_this_operation"/>
    <d:sourcevariable dataType="OpcUa_Double" name="voltage" addressSpaceRead="asynchronous" addressSpaceReadUseMutex="no" addressSpaceWrite="asynchronous" addressSpaceWriteUseMutex="no"/>

i.e.:
<d:class name="DacOutput">
    <d:devicelogic/>
    <d:sourcevariable dataType="OpcUa_Double" name="voltage" 
    addressSpaceRead="asynchronous" addressSpaceReadUseMutex="no" 
    addressSpaceWrite="asynchronous" addressSpaceWriteUseMutex="no"/>
    <d:configentry dataType="UaString" name="id">
</d:configentry>
  </d:class>

produces class methods read<Varname> and write<Varname> for scalars in Device/

if...
  		addressSpaceRead="forbidden" addressSpaceReadUseMutex="no"
  		addressSpaceWrite="forbidden" addressSpaceWriteUseMutex="no">
...no methods are generated, but generation is ignored even with xml errors (validation problem?)
forbidden, asynchronous, synchronous

IF code is generated, it ends up in AddressSpace/src/SourceVariables.cpp (sync and async)
as call to device methods read<Varname> and write<Varname>. 
SINCE these device methods are user specific they are maybe not yet written.
==> the AddressSpace/designToSourceVariablesBody.xslt
==> the AddressSpace/designToSourceVariablesHeader.xslt
must be adapted to include the new array calls in their threads
API as before: 
write call by value: vector<OPcUa_XX>
read call by ref vector<OpcUa_XX>&

...ahhhh... THIS is the sourceVariableThreadPool number !!

==>IN fact, just the signatures of the array method calls have to be generated in the IoJob-threads
and for this we need to detect if we have an array in the xml or not, as before: the scalar-array switch
==> let's do the execute-try block already

need vector to UaVariant encapsulation as for cache vars, for this:
1. generate min/maximumSize methods from design
2. use them in data copy to UaVariant



configured cach vars: populated from config.xml
validateed by Configuration.xsd
Configuration.xsd is generated by designToConfigurationXSD.xslt
a. adding xs:Array type to xslt->xsd
have to automatically build up the complex type of the class


the whole configuration and population of cache vars is unclear.
also the config vars are unclear
-arrays have to be members and NOT attributes
-ok, manage to create a valid Configuration.xsd with arrays as class elements

-Configuration constructor body in AS is not array aware yet
/home/mludwig/quasar-array-demo4/quasar/demo4/AddressSpace/src/ASDEMO4.cpp: In constructor ‘AddressSpace::ASDEMO4::ASDEMO4(UaNodeId, const UaNodeId&, AddressSpace::ASNodeManager*, const Configuration::DEMO4&)’:
/home/mludwig/quasar-array-demo4/quasar/demo4/AddressSpace/src/ASDEMO4.cpp:116:5: error: no matching function for call to ‘UaVariant::setDouble(const cache_array_double_type&)’
designToClassBody.xslt:185: fill in the vector loader. It is maybe possible to 
wrap that kind of xslt code up in a function: load a vector into a variant. NO IT IS NOT

todo: take away the array attribute in config.xml, as generated Configuration.xsd prescribes. There are elements. OK done


in config.xml, we would like to have:
	<cache_array_double>
		<value>100.3</value>
		<value>101.1</value>
		<value>102.2</value>
	</cache_array_double>

take a deep look at config variables. They are not in the AS space, but they have a config method
based on a generated struct to be accessed from user code. 





todo: string arrays create memory corruption. Conversion from UaString to OpcUa_String is doubtful
todo: sourcevariables as string arrays


testing
=======
cache vars arrays delegated: method call for arrays is wrong (it is scalar, but it should be vecor<>), 
works like source variables
ok.. hevy xml but seems OK... testing...



