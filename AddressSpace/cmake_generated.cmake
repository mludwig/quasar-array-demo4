

	add_custom_command(OUTPUT ${PROJECT_SOURCE_DIR}/AddressSpace/include/ASDEMO4.h ${PROJECT_SOURCE_DIR}/AddressSpace/src/ASDEMO4.cpp 
	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
	COMMAND python quasar.py generate asclass DEMO4
	DEPENDS ${DESIGN_FILE} ${PROJECT_SOURCE_DIR}/AddressSpace/designToClassHeader.xslt ${PROJECT_SOURCE_DIR}/AddressSpace/designToClassBody.xslt Configuration.hxx validateDesign
	)	
	
	
	
	set(ADDRESSSPACE_CLASSES 
	
	src/ASDEMO4.cpp
	
	)
	
	set(ADDRESSSPACE_HEADERS
	
	include/ASDEMO4.h
	
	)
	

	
	